package projektOJP3;

public class Magazine extends Book implements myInterface{
	private int isbn;
	private String title, publisher;
	private double price;
	
	public Magazine(){
		isbn = 0;
		title = null;
		publisher = null;
		price = 0;
	}
	public Magazine(int isbn, String title, String publisher, double price){
		this.isbn = isbn;
		this.publisher = publisher;
		this.title = title;
		this.price = price;
	}
	
	@Override
	public String toString(){
		System.out.println(chose());
		return "\nMAGAZINE:\nTitle: " + title + "\nAuthor: " + publisher + "\nISBN: " + isbn + "\nPrice: " +price +"\n";
	}
	@Override
	public String chose() {
		String chosenMagazine = "You're using now the Magazine shelf";
		return chosenMagazine;
	}
}
