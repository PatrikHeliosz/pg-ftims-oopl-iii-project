package projektOJP3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TheMainClass {
	public static TheMainClass library;
	static String fileName = null;
	static Library lib = new Library();
	static Scanner in = new Scanner(System.in);
	static Boolean run = true;

	public static void main(String[] args) {

		while (run) {
			System.out.println("\n0 for loading the library.");
			System.out.println("1 for saving and quitting");
			System.out.println("2 for listing all books in library");
			System.out.println("3 for adding book/magazine to library");
			System.out.println("4 for viewing all book types in library");

			boolean fine = true;
			int answer = 5;

			do {
				try {
					answer = in.nextInt();
					fine = false;
				} catch (Exception e) {
					System.out.println("\nEnter a number from 0 to 4\n");
				}
			} while (fine);

			if (answer == 0) {
				System.out.println("\nEnter file name to load");
				loadScript(in.next());
			} else if (answer == 1) {
				saveAndQuit();
			} else if (answer == 2) {
				System.out.println("\n\n" + lib.toStringMagazine());
				System.out.println(lib.toStringBook());
			} else if (answer == 3) {
				addBook();
			} else if (answer == 4) {
				lib.seeTypes();
			}
		}
	}

	private static void addBook() {
		int isbn = 0;
		String title = "null", author = "null", type = "null", publisher = "null";
		double price = 0.0;

		System.out.println("do you want to add a book(1) or a magazine(2)?");

		boolean fine = true;
		int enteredKey = 0;
		
		do {//it's like a custom exception handler without going into exceptions
			enteredKey = in.nextInt();
			fine = false;
			if(enteredKey == 1 || enteredKey == 2)
			{
				fine = false;
			}
			else{
				System.out.println("\nEnter 1 or 2\n");
			}
		} while (fine);

		if (enteredKey == 1) {
			fine = true;

			do {
				try {
					System.out.println("\nEnter the title: ");
					title = in.next();

					System.out.println("\nEnter the author: ");
					author = in.next();

					System.out.println("\nEnter the isbn: ");
					isbn = in.nextInt();

					System.out.println("\nEnter the price: ");
					price = in.nextDouble();

					System.out.println("\nEnter the type: ");
					type = in.next();

					fine = false;

				} catch (Exception e) {
					System.out.println("Well damn... you've typed in some wrong characters, try again");
				}

				Book b = new Book(isbn, title, author, price);

				BookType b1 = new BookType(type, b);
				lib.addType(type);
				lib.addBook(b1);
			} while (fine);

		} else if (enteredKey == 2) {
			fine = true;
			do {
				try {
					System.out.println("\nEnter the title: ");
					title = in.next();

					System.out.println("\nEnter the publisher: ");
					publisher = in.next();

					System.out.println("\nEnter the isbn: ");
					isbn = in.nextInt();

					System.out.println("\nEnter the price: ");
					price = in.nextDouble();

					System.out.println("\nEnter the type: ");
					type = in.next();

					fine = false;

				} catch (Exception e) {
					System.out.println("Well damn... you've typed in some wrong characters, try again");
				}

				Magazine b = new Magazine(isbn, title, publisher, price);

				MagazineType b1 = new MagazineType(type, b);
				lib.addType(type);
				lib.addMagazine(b1);
			} while (fine);
		}
	}

	private static void saveAndQuit() {
		System.out.println("\nEnter file name: ");
		fileName = in.next() + ".ser";
		run = false;
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		try {
			fos = new FileOutputStream(fileName);
			out = new ObjectOutputStream(fos);
			out.writeObject(lib);
			fos.close();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		catch (IOException e) {
			e.printStackTrace();
		}

		System.exit(0);
	}

	private static void loadScript(String name) {
		FileInputStream fis = null;
		ObjectInputStream in = null;
		File file = new File(name + ".ser");

		if (file.exists()) {
			try {
				fis = new FileInputStream(file);
				in = new ObjectInputStream(fis);
				lib = (Library) in.readObject();
				fis.close();
				in.close();

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				throw new CustomException();
			} catch (CustomException ex) {
				System.out.println("Ooops... The file is nonexistent... \nTry again or type another one");
			}
		}

	}

}
