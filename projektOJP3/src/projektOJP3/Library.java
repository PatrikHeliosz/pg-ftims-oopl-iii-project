package projektOJP3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.io.Serializable;

public class Library extends Object implements Serializable, myInterface, myInterface2{
	private List<Book> collection;//list of books
	private List<Magazine> collection2;//list of magazines
	private Set<String> types;//list of existing types
	
	public Library(){
		collection = new ArrayList<Book>();
		collection2 = new ArrayList<Magazine>();
		types  = new HashSet<String>() ;
	}
	
	public void addType(String type1){//adding book types
		types.add(type1);
	}
	
	public void seeTypes(){//viewing book types
		System.out.println(chose());
		if(!types.isEmpty()){
			System.out.println(types);	
		}
		else{
			System.out.println("Sorry... yet there are no types in library aviable");
		}
		
	}
	
	public void addBook(Book book){//actual adding books
		collection.add(book);
		System.out.println(chose());

		System.out.println(adding());
	}
	public void addMagazine(Magazine magazine){//actual adding magazines
		collection2.add(magazine);
		System.out.println(chose());

		System.out.println(adding());
	}
	
	
	
	public String toStringBook(){
		System.out.println(chose());
		String total = "\n";
		
		Iterator<Book> i = collection.iterator();
		while(i.hasNext()){
			Book b = (Book) i.next();
			total = total + b.toString();
		}
		return total;
	}
	
	public String toStringMagazine(){
		System.out.println(chose());
		String total = "\n";
		
		Iterator<Magazine> i = collection2.iterator();
		while(i.hasNext()){
			Magazine b = (Magazine) i.next();
			total = total + b.toString();
		}
		return total;
	}
	
	@Override
	public String chose() {
		String chosenLibrary = "You're using now the Library";
		return chosenLibrary;
	}
	@Override
	public String adding() {
		String addingData = "You added the entered data to the library";
		return addingData;
	}
}
