package projektOJP3;

import java.io.Serializable; 

public class Book implements Serializable, myInterface{
	private int isbn;
	private String title, author;
	private double price;
	
	public Book(){
		isbn = 0;
		title = null;
		author = null;
		price = 0;
	}
	public Book(int isbn, String author, String title, double price){
		this.isbn = isbn;
		this.author = author;
		this.title = title;
		this.price = price;
	}
	
	@Override
	public String toString(){
		System.out.println(chose());
		return "\nBOOK\nTitle: " + title + "\nAuthor: " + author + "\nISBN: " + isbn + "\nPrice: " +price +"\n";
	}
	@Override
	public String chose() {
		String chosenBook = "You're using now the Book shelf";
		return chosenBook;
	}
}
