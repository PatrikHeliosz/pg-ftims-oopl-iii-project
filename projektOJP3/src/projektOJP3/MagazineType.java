package projektOJP3;

public class MagazineType extends Magazine {
	private String magazineType;
	private Magazine magazine;
	
	public MagazineType(){
		magazine = null;
		magazineType = null;
	}
	
	public MagazineType(String magazineType, Magazine magazine){
		this.magazineType = magazineType;
		this.magazine = magazine;
	}
	
	@Override
	public String toString(){
		return magazine.toString() + "Magazine type: " + magazineType;
	}
}
