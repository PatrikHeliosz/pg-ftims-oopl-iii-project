package projektOJP3;

public class BookType extends Book{
	private String bookType;
	private Book book;
	
	public BookType(){
		book = null;
		bookType = null;
	}
	
	public BookType(String bookType, Book book){
		this.bookType = bookType;
		this.book = book;
	}
	
	@Override
	public String toString(){
		return book.toString() + "Book type: " + bookType;
	}
}
