author of project: Patrik Heliosz


ENGLISH:
The application is a library simulator. You can add and look after books or magazines that are stored in the library. After adding all the books you need, you can save the work to a ".ser" file, so you don't lose the data. 

The source code works was made in Eclipse, as you compile the code, no window appears, the program works within the console.

The application has basic exception handlers (example: writing letter's as for "price"). 

You can look through all the book types that are stored in the library. 


POLSKI:
Aplikacja jest symulatorem biblioteki. Można dodawać i przeglądać wszystkie książki i magazyny, które zostały w niej zapisane. Po dodaniu wszystkich książek można zapisać pracę w pliku z rozszerzeniem ".ser", wobec czego dane nie zostaną utracone.

Kod źródłowy napisany został w Eclipse, kompilując kod w nim, nie pojawia się okienko, aplikacja działa w konsoli programu.

Aplikacja ma podstawowe zabezpieczenia (np. przy wpisywaniu liter w miejsce "price" - cena).

Można przeglądać wszystkie gatunki książek, które znajdują się w bibliotece.


DEUTSCH:
Die App ist eine Simulation von einer Bibliothek. Mann kann Bücher und Magazine dazufügen und alle dort gespeicherte durchschauen. Nach fertiger Arbeit kann mann alles in einer .ser Datei speichern, so wird alles gesichert.

Der Code wurde in Eclipse geschrieben, nach Kompilieren gibt es kein pop-up Fenster, die App arbeitet in der Konsole des Programs.

Die App hat Basissicherung (z.B. nach einschreiben von Buchstaben wo price - also Preiß).

Mann kann durch alle Büchersorten durchschauen die in der Bilbiothek sind.








# Object-Oriented Programming Languages III final project #

This repository is for students of the Object-Oriented Programming Languages III course final laboratory project. Only through contribution via fork of this repository project will be considered submitted.

## Basic project rules

* Each student need to create a Java application using Java SE programming language.
* Projects can be submitted till January 19th, 11:59:59 PM (BitBucket server time). All contributions made after deadline will result in failing project (hence failing course).
* Submitting project before deadline if required to be admitted to oral exam.
* Not submitting project before deadline will result in failing laboratories (hence failing course).
* Subject (what the application actually does) it totally up to student. If for any reason student has a problem with choosing application subject, he or she can ask for assistance project verificators.
* Each student is obliged to create fork of this repository and make all contributions to it. Contributions made to original repository (f.e. pull requests) or send via other channels (like email) won't be accepted.
* Only source code, resources and so should be pushed to repository.
* Each project needs to be documented by README.md file (edited version of this one). Documentation needs to contain:
	* project author name,
	* description of what project does (in just a few sentences),
	* instructions how to compile and run application,
	* all additional information needed for application usage.

## Requirements for project

* All source code needs to be created regarding [Java Code Conventions](http://www.oracle.com/technetwork/java/codeconventions-150003.pdf).
* Project needs to have at least 2-level class inheritance present (regardless if base class is abstract or not).
* Example of using class field polymorphic binding needs to be present in the source code.
* Usage of at least two custom interfaces is mandatory.
* Project code needs to use two types of collections: List or Set and Map. Collections implementation is for student to decide.
* Example of collection iteration needs to be present in the source code.
* Custom exception needs to be created used in the project.

## Project verificators

* Dr. Jan Franz
* Dr. hab. Julien Guthmuller

## Forking repository

All needed information about forking a repository hosted on BitBucket can be found in the [documentation](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).

If there is a problem, please contact project verificators via email or during laboratories.